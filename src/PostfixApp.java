import java.util.Scanner;

public class PostfixApp {
    public static void main(String[] args) {
        String input;
        int output;
        
        while(true) {
            System.out.print("Enter postfix: ");
            Scanner kb = new Scanner(System.in);
            input = kb.nextLine();
            if(input.equals("")) break;
            ParsePost aParser = new ParsePost(input);
            output = aParser.doParse();
            System.out.println("Evaluates to " + output);
        }
    }

}
